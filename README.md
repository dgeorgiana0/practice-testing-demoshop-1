# DEMO shop Test Framework

## Technology
- _Java_
- _Allure Reports_
- _Selenide_

## Description
In acest framework au fost implementate 38 de teste incluse in 6 clase de teste. Testele au fost aplicate pe un site de tip magazin online. Au fost testate urmatoarele aspecte:

1. Login
2. Adaugare produse in cos
3. Interactiunea cu produsele din cos
4. Navigarea de pe o pagina pe alta a site-ului
5. Sortarea produselor
6. Logarea unui utilizator si interactiunea acestuia cu site-ul


![raport](raport.jpg)