package org.fasttrackit;


import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

@Feature("User interaction on the cart page in demo shop.")
@Owner("Raluca Draghici")
@Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
@Epic("Cart interactions")
@Severity(SeverityLevel.NORMAL)
public class CartManagementTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();

    @BeforeClass
    public void setup() {
        page.openHomepage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
        header.clickOnTheShoppingBagIcon();
    }

    @Test(description = "When user navigates to the Cart page, message is displayed")
    public void when_user_navigates_to_cart_page_empty_cart_page_message_is_displayed() {
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getEmptyCartPage(), "How about adding some products in your cart?");
    }

    @Test(description = "When user adds a product in cart and navigates to the Cart page, message isn't displayed")
    public void when_adding_one_product_to_cart_empty_cart_message_is_not_displayed() {
        Product product = new Product("6");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed(),"Product added to cart is displayed on the page");
    }

    @Test(description = "User can increase the amount of a product in cart page")
    public void user_can_increase_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("6");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        ProductInCart item = new ProductInCart("6");
        item.addAmount();
        assertEquals(item.getItemAmount(),"2", "The amount of a product is 2.");
    }

    @Test(description = "User can decrease the amount of a product in cart page")
    public void user_can_decrease_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("6");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        ProductInCart item = new ProductInCart("6");
        item.reduceAmount();
        assertEquals(item.getItemAmount(),"1", "The amount of a product is 1.");
    }

    @Test(description = "User can delete a product from cart page")
    public void user_can_delete_a_product_from_the_cart_page() {
        Product product = new Product("6");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        ProductInCart item = new ProductInCart("6");
        item.deleteProduct();
        assertEquals(cartPage.getEmptyCartPage(),"How about adding some products in your cart?");
    }
}
