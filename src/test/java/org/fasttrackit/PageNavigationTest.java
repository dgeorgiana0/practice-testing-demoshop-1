package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("User navigation on demo shop pages.")
@Owner("Raluca Draghici")
@Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
@Epic("Pages Navigation")
@Severity(SeverityLevel.CRITICAL)

public class PageNavigationTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();
    CheckoutPage checkoutPage = new CheckoutPage();


    @BeforeClass
    public void setup() {
        page.openHomepage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }
    @Test(description = "User can navigate to the Wishlist Page")
    public void user_can_navigate_to_Wishlist_Page() {
        header.clickOnTheWishListIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on Wishlist page.");
    }
    @Test(description = "User can navigate to the Cart Page")
    public void user_can_navigate_to_Cart_page() {
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart page.");
    }
    @Test(description = "User can navigate to the Homepage from Wishlist Page")
    public void user_can_navigate_to_Home_Page_from_Wishlist_Page() {
        header.clickOnTheWishListIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Wishlist page.");
    }

    @Test(description = "User can navigate to the Homepage from Cart page")
    public void user_can_navigate_to_Home_Page_from_Cart_page() {
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Home Page.");
    }
    @Test(description = "User can navigate to the Cart Page from Wishlist Page")
    public void user_can_navigate_to_Cart_Page_from_Wishlist_Page() {
        header.clickOnTheWishListIcon();
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on Cart page.");
    }

    @Test(description = "User can navigate to the Checkout Page from Cart Page")
    public void user_can_navigate_to_Checkout_Page_from_Cart_Page() {
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        assertEquals(page.getPageTitle(), "Your information", "Expected to be on Checkout page.");
    }

    @Test(description = "User can continue shopping from Cart Page")
    public void after_adding_product_to_cart_user_can_navigate_to_Home_Page_from_Cart_Page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheContinueShoppingButton();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Home Page.");
    }
    @Test(description = "User can navigate to the Homepage from Checkout Page")
    public void user_can_navigate_to_Home_Page_from_Checkout_Page() {
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Home page.");
    }


    @Test(description = "User can navigate to order complete from Cart Page")
    public void user_can_navigate_to_Order_complete_Page_from_Cart_Page() {
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("Ana");
        modal.typeInLastName("Popescu");
        modal.typeInAddress("Str. Florilor nr 155");
        header.clickOnTheCheckoutButton();
        assertEquals(page.getPageTitle(), "Order summary", "Expected to be on Order summary.");
        header.clickOnTheCheckoutButton();
        assertEquals(checkoutPage.getCompleteCheckout(), "Thank you for your order!");
    }

    @Test(description = "User can't complete order without Last Name field being completed")
    public void when_user_leaves_Last_Name_information_empty_error_message_appears() {
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("Andrei");
        modal.typeInAddress("Str. Margaretei nr 100");
        header.clickOnTheCheckoutButton();
        assertEquals(modal.getErrorMessage(), "Last Name is required", "User cannot complete order until all your information fields are completed.");
    }

    @Test(description = "User can't complete order without Address field being completed")
    @Ignore
    public void when_user_leaves_Address_Field_empty_error_message_appears() {
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("John");
        modal.typeInLastName("Doe");
        header.clickOnTheCheckoutButton();
        assertEquals(modal.getErrorMessage(), "Address is required", "User cannot complete order until all your information fields are completed.");
    }

    @Test(description = "User can't complete order without all fields from your address are completed")
    public void when_user_leaves_your_information_fields_empty_error_message_appears() {
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        header.clickOnTheCheckoutButton();
        assertEquals(modal.getErrorMessage(), "First Name is required", "User cannot complete order until all your information fields are completed.");
    }

    @Test(description = "User can't complete order without valid name, last name and address are completed")
    public void when_user_introduces_invalid_information_in_your_information_fields_error_message_appears() {
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("0000");
        modal.typeInLastName("11111");
        modal.typeInAddress("2222");
        header.clickOnTheCheckoutButton();
        header.clickOnTheCheckoutButton();
        assertTrue(modal.isErrorMessageDisplayed(),"Error message displayed");
    }
}
