package org.fasttrackit;

import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Link;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Logged in user")
@Owner("Raluca Draghici")
@Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")

public class UserBeetlePagesInteractionTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();
    ProductCards productList = new ProductCards();
    CartPage cartPage = new CartPage();


    @BeforeClass
    public void setup() {
        page.openHomepage();
        header.clickTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
    }

    @Test(description = "User beetle can sort products alphabetically from A to Z")

    @Story("Sorting products")
    public void when_sorting_products_a_to_z_products_are_sorted_alphabetically_ASC() {
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheAZSortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();
        assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
        assertEquals(lastProductBeforeSort.getTitle(), lastProductAfterSort.getTitle());
    }

    @Test(description = "User beetle can sort products alphabetically from Z to A")

    @Story("Sorting products")
    @Issue("DMS-001")
    public void when_sorting_products_z_to_a_products_are_sorted_alphabetically_DESC() {
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheZASortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();
        assertEquals(firstProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
    }

    @Test(description = "User beetle can add any product to cart from product cards")

    @Story("Adding products to cart")
    @Issue("DMS-002")
    public void user_beetle_can_add_any_product_to_cart_from_product_cards() {
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }

    @Test(description = "User beetle can increase the amount of a product in cart page")

    @Story("Interaction with cart page")
    public void user_beetle_can_increase_the_amount_of_a_product_in_cart_page() {
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("2");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        ProductInCart item = new ProductInCart("2");
        item.addAmount();
        assertEquals(item.getItemAmount(), "2", "The amount of a product is 2.");
        item.deleteProduct();
    }

    @Test(description = "User beetle can reduce the amount of a product in cart page")

    @Story("Interaction with cart page")
    public void user_beetle_can_reduce_the_amount_of_a_product_in_cart_page() {
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Home page.");
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("2");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        ProductInCart item = new ProductInCart("2");
        item.reduceAmount();
        assertEquals(item.getItemAmount(), "1", "The amount of a product is 1.");
    }

    @Test(description = "User beetle can delete a product from cart page")

    @Story("Interaction with cart page")
    public void user_beetle_can_delete_a_product_from_the_cart_page() {
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Home page.");
        Product product = new Product("2");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        ProductInCart item = new ProductInCart("2");
        item.deleteProduct();
        assertEquals(cartPage.getEmptyCartPage(), "How about adding some products in your cart?");
    }

    @Test(description = "User beetle can navigate to the Checkout Page from Cart Page")

    @Story("Interaction with cart page")
    public void user_beetle_can_navigate_to_Checkout_Page_from_Cart_Page() {
        header.clickOnTheShoppingBagIcon();
        Product product = new Product("2");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        ProductInCart item = new ProductInCart("2");
        header.clickOnTheCheckoutButton();
        assertEquals(page.getPageTitle(), "Your information", "Expected to be on Checkout page.");
        header.clickOnTheContinueShoppingButton();
        item.deleteProduct();
    }

    @Test(description = "User beetle can logout")

    @Story("User can logout")
    public void user_beetle_can_logout() {
        header.clickTheLogOutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Logged in with beetle, expected greetings message to be Hello guest!");
        header.clickTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
    }
}
