package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("User can add products in cart.")
@Owner("Raluca Draghici")
@Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
@Epic("Add to cart")
@Severity(SeverityLevel.CRITICAL)

public class AddProductToCartTest {
    Page page = new Page();
    Header header = new Header();

    @BeforeClass
    public void setup() {
        page.openHomepage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }

    @Test(description = "User can add product to cart from product cards")

    public void user_can_add_product_to_cart_from_product_cards() {
        Product product = new Product("6");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }

    @Test(description = "User can add products to cart from product cards")
    public void user_can_add_two_products_to_cart_from_product_cards() {
        Product product = new Product("6");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two product to cart, badge shows 2.");
    }

}
