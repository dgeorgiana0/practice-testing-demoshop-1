package org.fasttrackit;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Link;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Login")
@Severity(SeverityLevel.CRITICAL)
@Owner("Raluca Draghici")
@Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")

public class LoginTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomepage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }
    @Test(description = "User turtle can login with valid credentials")
    @Description("User turtle can login with valid credentials.")
    @Story("Login with valid credentials")
    public void user_turtle_can_login_with_valid_credentials() {
        header.clickTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi turtle!", "Logged in with turtle, expected greetings message to be Hi turtle!");
    }

    @Test(description = "User dino can login with valid credentials")
    @Description("User dino can login with valid credentials.")
    @Story("Login with valid credentials")
    public void user_dino_can_login_with_valid_credentials() {
        header.clickTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi dino!", "Logged in with dino, expected greetings message to be Hi dino!");
    }

    @Test(description = "User beetle can login with valid credentials")
    @Description("User beetle can login with valid credentials.")
    @Story("Login with valid credentials")
    public void user_beetle_can_login_with_valid_credentials() {
        header.clickTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi beetle!", "Logged in with beetle, expected greetings message to be Hi beetle!");
    }
    @Test(description = "Log in with invalid credentials")
    @Description("User can't login with invalid credentials.")
    @Story("Login with invalid credentials")
    public void when_user_logs_in_with_invalid_credentials_error_message_appears() {
        header.clickTheLoginButton();
        modal.typeInUsername("betle");
        modal.typeInPassword("choo");
        modal.clickOnTheLoginButton();
        assertEquals(modal.getErrorMessage(), "Incorrect username or password!", "User cannot login in with invalid credentials.");
        modal.clickTheCloseLoginDialogButton();
    }

    @Test(description = "Log in with invalid username and valid password")
    @Description("User can't login with invalid username.")
    @Story("Login with invalid credentials")
    public void when_user_logs_in_with_invalid_username_error_message_appears() {
        header.clickTheLoginButton();
        modal.typeInUsername("betle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(modal.getErrorMessage(), "Incorrect username or password!", "User cannot login in with invalid credentials.");
        modal.clickTheCloseLoginDialogButton();
    }
    @Test(description = "Log in with valid username and invalid password")
    @Description("User can't login with valid username and invalid password.")
    @Story("Login with invalid credentials")
    public void when_user_logs_in_with_invalid_password_error_message_appears() {
        header.clickTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("dino");
        modal.clickOnTheLoginButton();
        assertEquals(modal.getErrorMessage(), "Incorrect username or password!", "User cannot login in with invalid credentials.");
        modal.clickTheCloseLoginDialogButton();
    }

}
