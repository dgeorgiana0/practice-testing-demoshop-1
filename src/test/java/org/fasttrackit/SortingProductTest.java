package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
@Feature("Sort")
@Owner("Raluca Draghici")
@Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
@Story("Sorting products from list")
@Severity(SeverityLevel.MINOR)
public class SortingProductTest {
    Page page = new Page();
    ProductCards productList = new ProductCards();

    @BeforeClass
    public void setup() {
        page.openHomepage();
    }
    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }

    @Test(description = "User can sort products alphabetically from A to Z")
    public void when_sorting_products_a_to_z_products_are_sorted_alphabetically_ASC() {
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheAZSortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
        assertEquals(lastProductBeforeSort.getTitle(), lastProductAfterSort.getTitle());
    }

    @Test(description = "User can sort products alphabetically from Z to A")
    public void when_sorting_products_z_to_a_products_are_sorted_alphabetically_DESC() {
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheZASortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
    }

    @Test(description = "User can sort products ascending from low to high")
    public void when_sorting_products_by_price_low_to_high_products_are_sorted_by_price_low_to_high() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLowToHigh();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLowToHigh();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
    }

    @Test(description = "User can sort products descending from high to low")
    public void when_sorting_products_by_price_high_to_low_products_are_sorted_by_price_high_to_low() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLowToHigh();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceHighToLow();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
    }
}
