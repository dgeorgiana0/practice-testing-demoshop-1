package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;


public class ProductCards {
    private final ElementsCollection cards = $$(".card");
    private final SelenideElement sortButton = $(".sort-products-select");
    private final SelenideElement sortAZ = $("option[value=az]");
    private final SelenideElement sortZA = $("option[value=za]");
    private final SelenideElement sortLH = $("option[value=lohi]");
    private final SelenideElement sortHL = $("option[value=hilo]");

    public ProductCards() {
    }

    @Step("Click on the Sort Button.")
    public void clickOnTheSortButton() {
        this.sortButton.click();
    }

    @Step("Click on the Sort A to Z Button from the list.")
    public void clickOnTheAZSortButton() {
        this.sortAZ.click();
    }

    @Step("Click on the Sort Z to A Button from the list.")
    public void clickOnTheZASortButton() {
        this.sortZA.click();
    }

    @Step("Click on the Sort by price High to Low from the list.")
    public void clickOnTheSortByPriceHighToLow() {
        this.sortHL.click();
    }

    @Step("Click on the Sort by price Low to High from the list.")
    public void clickOnTheSortByPriceLowToHigh() {
        this.sortLH.click();
    }

    public Product getFirstProductInList() {
        SelenideElement first = cards.first();
        return new Product(first);
    }

    public Product getLastProductInList() {
        SelenideElement last = cards.last();
        return new Product(last);
    }

}
