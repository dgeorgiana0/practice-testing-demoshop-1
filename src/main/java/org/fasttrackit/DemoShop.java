package org.fasttrackit;

public class DemoShop {
    public static void main(String[] args) {
        System.out.println("- = Demo Shop = -");
        System.out.println("1. User can login in with valid credentials.");
        Page page = new Page();
        page.openHomepage();
        Header header = new Header();
        header.clickTheLoginButton();
        ModalDialog modal = new ModalDialog();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("dino");
        System.out.println("Hi Dino is displayed in the header: " + isLogged);
        System.out.println("Greetings msg is: " + greetingsMessage);

        System.out.println("-------------------------");
        System.out.println("2. User can add product to cart from product cards.");
        page.openHomepage();



        System.out.println("--------------------------");
        System.out.println("3. User can navigate to Home Page from Wishlist Page.");
        page.openHomepage();
        header.clickOnTheWishListIcon();
        String pageTitle = page.getPageTitle();
        System.out.println("Expected to be on Wishlist page. Title is: " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Home Page. Title is: " + pageTitle);

        System.out.println("--------------------------");
        System.out.println("4. User can navigate to Home Page from Cart Page.");
        page.openHomepage();
        header.clickOnTheCartIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the cart page. Title is:  " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Home Page. Title is: " + pageTitle);
    }
}
