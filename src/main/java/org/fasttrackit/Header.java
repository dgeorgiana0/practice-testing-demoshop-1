package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement logOutButton = $(".fa-sign-out-alt");
    private final SelenideElement greetingsElement = $(".navbar-text span span");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges = $$(".shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");
    private final SelenideElement checkoutButton = $(".fa-angle-right");
    private final SelenideElement continueShoppingButton = $(".fa-angle-left");


    @Step("Click on the login button")
    public void clickTheLoginButton() {
        loginButton.click();
        System.out.println("Click on the Login button.");
    }

    @Step("Click on the logout button")
    public void clickTheLogOutButton() {
        logOutButton.click();
        System.out.println("Click on the Logout button");
    }

    public String getGreetingsMessage() {
        return greetingsElement.text();
    }

    @Step("Click on the Wishlist button")
    public void clickOnTheWishListIcon() {
        System.out.println("Click on the Wishlist button.");
        wishlistButton.click();
    }
    @Step("Click on the ShoppingBag button")
    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping bag icon.");
        homePageButton.click();
    }
    @Step("Click on the Checkout button")
    public void clickOnTheCheckoutButton() {
        System.out.println("Click on the Checkout button.");
        checkoutButton.click();
    }
    @Step("Click on the Continue Shopping button")
    public void clickOnTheContinueShoppingButton() {
        System.out.println("Click on the Continue Shopping button.");
        continueShoppingButton.click();
    }
    @Step("Click on the cart button")
    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart icon");
        cartIcon.click();
    }

    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }

    public boolean isShoppingBadgeVisible() {
        return !this.shoppingCartBadges.isEmpty();
    }

}
