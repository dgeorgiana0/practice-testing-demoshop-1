package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private final SelenideElement emptyCartPageElement = $(".text-center");

    public CartPage() {
    }

    public String getEmptyCartPage() {
        return this.emptyCartPageElement.text();
    }

    public boolean isEmptyCartMessageDisplayed() {
        return emptyCartPageElement.isDisplayed();
    }
}
