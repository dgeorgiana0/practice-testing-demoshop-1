package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutPage {
    private final SelenideElement completeCheckoutPageElement = $(".text-center");

    public CheckoutPage() {
    }

    public String getCompleteCheckout() {
        return this.completeCheckoutPageElement.text();
    }
}
