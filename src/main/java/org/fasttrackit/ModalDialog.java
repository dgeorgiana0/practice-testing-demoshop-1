package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class ModalDialog {

    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement errorMessage = $(".error");
    private final SelenideElement firstname = $("#first-name");
    private final SelenideElement lastname = $("#last-name");
    private final SelenideElement address = $("#address");
    private final SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");
    private final SelenideElement closeLoginDialogButton = $(".close");

    @Step("Type in username in the Username field.")
    public void typeInUsername(String user) {
        System.out.println("Click on the Username field.");
        username.click();
        System.out.println("Type in " + user);
        username.type(user);
    }
    @Step("Type in password in the Password field.")
    public void typeInPassword (String pass) {
        System.out.println("Click on the Password field.");
        password.click();
        System.out.println("Type in " + pass);
        password.type(pass);
    }

    public String getErrorMessage() {
        return errorMessage.text();
    }

    public boolean isErrorMessageDisplayed() {
        return errorMessage.isDisplayed();
    }

    @Step("Click on the login button.")
    public void clickOnTheLoginButton() {
        System.out.println("Click on the Login button.");
        loginButton.click();
    }

    @Step("Click on the close button")
    public void clickTheCloseLoginDialogButton() {
        closeLoginDialogButton.click();
        System.out.println("Click on the close button.");
    }
    @Step("Type in First name in the First name field.")
    public void typeInFirstName (String name) {
        System.out.println("Click on the First name field.");
        firstname.click();
        System.out.println("Type in " + name);
        firstname.type(name);
    }
    @Step("Type in Last name in the Last name field.")
    public void typeInLastName (String name) {
        System.out.println("Click on the Last name field.");
        lastname.click();
        System.out.println("Type in " + name);
        lastname.type(name);
    }
    @Step("Type in address in the Address name field.")
    public void typeInAddress (String addressname) {
        System.out.println("Click on the Address name field.");
        address.click();
        System.out.println("Type in " + addressname);
        address.type(addressname);
    }

}
